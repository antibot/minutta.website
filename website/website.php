﻿<?php
$LOCAL = $_SERVER['HTTP_HOST'] == 'localhost';

$BASE_URL = '//' . $_SERVER['HTTP_HOST'] . '/';
$SERVER_URL = $BASE_URL . 'api.php';

$TEMPLATE_URL = ($LOCAL ? 'template1.php?' : $BASE_URL . 'project.php?template=1&');

$SOUND_PREFIX = $BASE_URL . 'apidata/website/songs/';
$PHOTO_PREFIX = $BASE_URL;

$PREFIX = ($LOCAL ? '' : $BASE_URL) . 'apidata/website/';

// ---

if (!$LOCAL) {
    require_once 'NumCoder.php';
    require_once 'config.php';
}

// ---

$images = array();

$texts = array();

$sound = '';

$type = '';

if (!$LOCAL) {

    $ok = false;

    $vid = isset($_REQUEST['vid']) && !empty($_REQUEST['vid']) ? $_REQUEST['vid'] : NULL;
    if ($vid) {
        //$pid = NumCoder::decode($vid); //MinuttaServer::base_decode($vid);
        $pid = NumCoder::decodeDES($vid);
        error_log("coded $vid decode $pid");
    } else {
        $pid = isset($_REQUEST['pid']) && !empty($_REQUEST['pid']) ? $_REQUEST['pid'] : NULL;
    }

    if (!empty($pid)) {
        $db = MinuttaServer::InitDB();

        if (!empty($db)) {

            $q = "SELECT * FROM projects WHERE id='$pid' LIMIT 1";
            error_log($q);

            $res = $db->query($q);

            if (!empty($res)) {
                $data = $res->fetch_assoc();

                if (!empty($data)) {
                    $ok = true;

                    $type = intval($data['template_id']);

                    $sound = $SOUND_PREFIX . (intval($data['music_id']) + 1) . '.mp3';

                    if (isset($data['frames']) && !empty($data['frames'])) {
                        $frames = json_decode($data['frames']);

                        if (!empty($frames) && is_array($frames)) {
                            foreach ($frames as $frame) {
                                $images[] = $PHOTO_PREFIX . $frame;
                            }
                        }
                    }
                }
            } else {
                //echo 'DB error 2';
            }
        } else {
            //echo 'DB error 1';
        }
    } else {
        //echo 'Bad params';
    }

    if (!$ok) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }
}

// --- 

if (empty($images)) {
    $images = array();

    for ($i = 0; $i < 13; $i++) {
        $images[] = $PREFIX . "frames/" . ($i + 1) . ".JPG";
    }
}

if (empty($texts)) {
    $texts = array();

    $texts[] = array('index' => 'first', 'text' => '<small>1 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />');
    $texts[] = array('index' => 'last', 'text' => '"МИНУТТА С ЛЮБОВЬЮ"<br /><br /><small>ПОД ONE DIRECTION</small><br />');
}

if (empty($sound)) {
    $sound = $PREFIX . 'songs/2.mp3';
}

// --- 

$parameters = http_build_query(array('images' => $images, 'texts' => $texts, 'sound' => $sound));
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link href='http://fonts.googleapis.com/css?family=Exo+2:400,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?= $PREFIX; ?>css/normalize.css">
        <link rel="stylesheet" href="<?= $PREFIX; ?>css/main.css">
        <link rel="stylesheet" href="<?= $PREFIX; ?>css/base.css">

        <script src="<?= $PREFIX; ?>js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <script>
            window.GLOBAL_COMMENTS = [];
            window.GLOBAL_LIKES = [];

            window.GLOBAL_PREFIX = '<?= $PREFIX; ?>';
            
            window.GLOBAL_BASE_URL = '<?= $BASE_URL; ?>';
            window.GLOBAL_SERVER_URL = '<?= $SERVER_URL; ?>';

            window.GLOBAL_PID = '<?= isset($pid) ? $pid : 0; ?>';
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?= $PREFIX; ?>js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.transform2d.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.transform3d.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.placeholder.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.browser.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.json-2.4.min.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.mousewheel.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.transit.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/jquery.support.cssproperty.js"></script>
        <script src="<?= $PREFIX; ?>js/vendor/moment-with-langs.min.js"></script>
        <script src="<?= $PREFIX; ?>js/components/likes.js"></script>
        <script src="<?= $PREFIX; ?>js/components/comments.js"></script>
        <script src="<?= $PREFIX; ?>js/plugins.js"></script>
        <script src="<?= $PREFIX; ?>js/main.js"></script>
        <script src="<?= $PREFIX; ?>js/base.js"></script>

        <div id="m-content">
            <div id="m-content-block">
                <div id="m-content-left">
                    <div id="m-player">
                        <iframe id="m-template" src="<?= $TEMPLATE_URL . $parameters; ?>" width="100%" height="100%" frameborder="0"> 
                        </iframe>
                         
                        <div id="m-cover">
                            <img src="<?= $images[0]; ?>" />
                        </div>
                        
                        <div class="m-player-controls">
                            <div class="m-button-play"></div>
                            <div class="m-button-pause"></div>
                        </div><!-- m-player-controls --> 
                    </div><!-- m-player -->
                    <div id="m-player-left-arrow">
                        <div></div>
                        <span class="m-icon-container">
                            <span class="m-icon"></span>  
                        </span>
                    </div><!-- m-player-left-arrow -->
                    <div id="m-player-right-arrow">
                        <div></div>
                        <span class="m-icon-container">
                            <span class="m-icon"></span>  
                        </span>
                    </div><!-- m-player-right-arrow -->
                </div><!-- m-content-left -->
                <div id="m-content-right">
                    <div id="m-introduce">
                        <div class="m-logo">
                            <img src="<?= $PREFIX; ?>img/Minutta_logo.png" />
                        </div>

                        <a href="#" class="m-button-login">Log in</a>
                        <a href="#" class="m-button-logout">Log out</a>
                    </div>

                    <div id="m-panel">
                        <table>
                            <tr class="m-panel-row1">
                                <td>

                                </td>
                            </tr>
                            <tr class="m-panel-row2">
                                <td>
                                    <div class="m-user-information">
                                        <div class="m-title">Sunny days</div>
                                        <div class="m-author">by John Doe</div>
                                        <div class="m-geo"><span>Jūrmala, Latvija</span></div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="m-panel-row3">
                                <td>
                                    <div class="m-controls">
                                        <div class="m-button-play"></div>
                                        <div class="m-button-pause"></div>
                                        <div class="m-button-loading">
                                            <span class="m-text">loading...</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="m-panel-row4">
                                <td>
                                    <div class="m-slideshow-information">

                                    </div> 
                                </td>
                            </tr>
                            <tr class="m-panel-row5">
                                <td>
                                    <table class="m-actions">
                                        <tr>
                                            <td>
                                                <div class="m-action-comment m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Comment<span></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-action-like m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Like
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="#" target="_blank" class="m-action-download m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Download
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="m-panel-row6">
                                <td>
                                    <a href="#" class="m-button-applestore">
                                        <span class="m-icon"></span>
                                    </a>
                                    <a href="#" class="m-button-googleplay">
                                        <span class="m-icon"></span>
                                    </a> 
                                </td>
                            </tr>
                        </table>
                    </div><!-- m-panel -->

                    <div id="m-comments">
                        <table>
                            <tr class="m-comments-row1">
                                <td>

                                </td>
                            </tr>
                            <tr class="m-comments-row2">
                                <td>

                                </td>
                            </tr>
                            <tr class="m-comments-row3">
                                <td>
                                    <table class="m-actions">
                                        <tr>
                                            <td>
                                                <div class="m-action-comment m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Comment<span></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-action-like m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Like
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="#" target="_blank" class="m-action-download m-action">
                                                    <div class="m-icon"></div>
                                                    <div class="m-effect"></div>
                                                    <div class="m-text">
                                                        Download
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="m-comments-row4">
                                <td>
                                    <table class="m-comment-label">
                                        <tr>
                                            <td>No comments yet</td>
                                        </tr>
                                    </table>
                                    <ul class="m-comment-list">
                                    </ul>
                                </td>
                            </tr>
                            <tr class="m-comments-row5">
                                <td>

                                </td>
                            </tr>
                            <tr class="m-comments-row6">
                                <td>
                                    <div class="m-comment-inline">
                                        <input placeholder="Say something nice" value="" type="text" />
                                        <div class="m-comment-add"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div><!-- m-comments -->


                </div><!-- m-content-right -->
            </div><!-- m-content-block -->

        </div><!-- m-content -->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                    (function(b, o, i, l, e, r) {
                        b.GoogleAnalyticsObject = l;
                        b[l] || (b[l] = function() {
                            (b[l].q = b[l].q || []).push(arguments);
                        });
                        b[l].l = +new Date;
                        e = o.createElement(i);
                        r = o.getElementsByTagName(i)[0];
                        e.src = '//www.google-analytics.com/analytics.js';
                        r.parentNode.insertBefore(e, r);
                    }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X');
            ga('send', 'pageview');
        </script>
    </body>
</html>
