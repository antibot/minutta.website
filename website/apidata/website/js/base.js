(function($) {
    $(function() {
        // --- initialize

        var loaded = false;

        var player_left_arrow = $('#m-player-left-arrow'), player_right_arrow = $('#m-player-right-arrow');

        var button_play = $('#m-panel .m-button-play'), button_pause = $('#m-panel .m-button-pause'), button_loading = $('#m-panel .m-button-loading');

        var button_play_effect = $('#m-player .m-button-play'), button_pause_effect = $('#m-player .m-button-pause');

        var action_comment = $('.m-action-comment'), action_like = $('.m-action-like'), action_download = $('.m-action-download');

        var slideshow_information = $('.m-slideshow-information');
        var slideshow_information_template = 'photo %1 of %2';

        var comment_inline = $('#m-comments .m-comment-inline');
        var comment_add = $('#m-comments .m-comment-add');

        var panel = $('#m-panel');

        var comments = $('#m-comments');

        var cover = $('#m-cover');

        var frame = $('#m-template');

        var template = frame[0].contentWindow;

        var css3 = $.support.cssProperty('transition') && $.support.cssProperty('transform');

        // --- options

        $('input, textarea').placeholder();

        // --- functions

        window.player = window.player || {};

        window.player.play = function(effect) {
            effect = effect === undefined ? true : effect;

            template.play();

            if (effect) {
                button_play.fadeOut('fast');
                button_pause.fadeIn('fast');
                player_left_arrow.fadeOut('fast');
                player_right_arrow.fadeOut('fast');
                button_play_effect.stop(true, true).animate({transform: 'scale(1)', opacity: '0.5'}, 0).show().animate({transform: 'scale(2)', opacity: '0'}, 'fast', function() {

                });
                button_pause_effect.hide();

                cover.fadeOut('fast', function() {
                    cover.hide();
                });

                frame.fadeIn('fast');
            } else {
                button_play.hide();
                button_pause.show();
                player_left_arrow.hide();
                player_right_arrow.hide();
                button_play_effect.hide();
                button_pause_effect.hide();

                cover.hide();

                frame.show();
            }
        };
        window.player.pause = function(effect) {
            effect = effect === undefined ? true : effect;

            if (effect) {
                button_play.fadeIn('fast');
                button_pause.fadeOut('fast');
                player_left_arrow.fadeIn('fast');
                player_right_arrow.fadeIn('fast');
                button_play_effect.hide();
                button_pause_effect.stop(true, true).animate({transform: 'scale(1)', opacity: '0.5'}, 0).show().animate({transform: 'scale(2)', opacity: '0'}, 'fast', function() {

                });
            } else {
                button_play.show();
                button_pause.hide();
                player_left_arrow.show();
                player_right_arrow.show();
                button_play_effect.hide();
                button_pause_effect.hide();
            }

            template.pause();
        };
        window.player.next = function() {
            //cover.hide();

            if (css3) {
                cover.transition({
                    x: -frame.width() + 'px',
                    queue: false,
                    duration: 500,
                    easing: 'in-out',
                    complete: function() {
                        $(this).remove();
                    }
                });
            } else {
                cover.fadeOut('fast');
            }

            template.next();
        };
        window.player.prev = function() {
            //cover.hide();

            if (css3) {
                cover.transition({
                    x: frame.width() + 'px',
                    queue: false,
                    duration: 500,
                    easing: 'in-out',
                    complete: function() {
                        $(this).remove();
                    }
                });
            } else {
                cover.fadeOut('fast');
            }

            template.prev();
        };
        window.player.index = function(position, count) {
            slideshow_information.fadeIn('fast').html(slideshow_information_template.replace('%1', position).replace('%2', count));
        };
        window.player.actionComment = function(selected) {
            console.log("!!!", selected);

            player.state(selected ? 1 : 0);
        };
        window.player.actionLike = function(selected) {

        };
        window.player.actionDownload = function(selected) {

        };
        window.player.state = function(index, effect) {
            effect = effect === undefined ? true : effect;

            console.log("!!!", index);

            switch (index) {
                case 0:
                    if (effect) {
                        panel.fadeIn('fast');
                        comments.fadeOut('fast');
                    } else {

                        panel.show();
                        comments.hide();
                    }

                    break;
                case 1:
                    if (effect) {
                        panel.fadeOut('fast');
                        comments.fadeIn('fast');
                    } else {
                        panel.hide();
                        comments.show();
                    }
                    break;
                default:
                    break;
            }
        };

        // --- events

        player_left_arrow.bind('click', function() {
            console.log('player_left_arrow');

            player.prev();
        });

        player_right_arrow.bind('click', function() {
            console.log('player_right_arrow');

            player.next();
        });

        button_play.bind('click', function() {
            console.log('button_play');

            if (!loaded) {
                return;
            }

            var self = $(this);

            self.addClass('sonarEffect').addClass('sonarEffect').addClass('exec');

            setTimeout(function() {
                self.removeClass('exec');
            }, 300);

            player.play();
        });

        button_pause.bind('click', function() {
            console.log('button_pause');

            if (!loaded) {
                return;
            }

            var self = $(this);

            self.addClass('exec');

            setTimeout(function() {
                self.removeClass('exec');
            }, 300);

            player.pause();
        });

        action_comment.bind('click', function() {
            console.log('action_comment');

            if (!loaded) {
                return;
            }

            action_comment.toggleClass('selected');

            var self = $(this).find('.m-effect');

            self.addClass('sonarEffect').addClass('exec');

            setTimeout(function() {
                self.removeClass('exec');
            }, 300);

            player.actionComment(action_comment.hasClass('selected'));
        });

        action_like.bind('click', function() {
            console.log('action_like');

            if (!loaded) {
                return;
            }

            action_like.toggleClass('selected');

            var self = $(this).find('.m-effect');

            self.addClass('sonarEffect').addClass('exec');

            setTimeout(function() {
                self.removeClass('exec');
            }, 300);

            player.actionLike(action_like.hasClass('selected'));
        });

        action_download.bind('click', function() {
            console.log('action_download');

            if (!loaded) {
                return;
            }

            var self = $(this).find('.m-effect');

            self.addClass('sonarEffect').addClass('exec');

            setTimeout(function() {
                self.removeClass('exec');
            }, 300);

            player.actionDownload(false);
        });

        comment_inline.bind('keyup', function(event) {
            switch (event.keyCode) {
                case 13: // enter

                    break;

                default:
                    break;
            }
        });

        comment_add.bind('click', function() {

        });

        // --- make 

        var interval = setInterval(function() {
            // --- Определяем позицию и количество фотографий

            if (template && template.hasOwnProperty('initialized')) {
                if (!loaded && template.loaded()) {
                    loaded = true;

                    player_left_arrow.fadeIn('fast');
                    player_right_arrow.fadeIn('fast');

                    button_play.fadeIn('fast');
                    button_loading.fadeOut('fast');
                }

                if (template.current()) {
                    action_download.attr('href', template.current().src);
                } else {

                }

                if (template.initialized) {
                    player.index(template.position() + 1, template.count());
                } else {
                    player.index(0, 0);
                }
            } else {
                player.index(0, 0);
            }
        }, 100);

    });
})(jQuery);