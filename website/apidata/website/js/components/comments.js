(function($) {
    $(function() {

        var action_comment = $('.m-action-comment');

        action_comment.bind('click', function() {
            console.log('action_comment');
        });

        // ---

        var ModelComment = function() {
            var self = this;

            return self;
        };

        var Comments = function(options) {
            options = options || {};

            var self = this;

            // --- private

            var _lastComment = null;

            // --- constants

            var CONST_BASE_URL = '{BASE_URL}';
            var CONST_AUTHOR = '{AUTHOR}';
            var CONST_TEXT = '{TEXT}';
            var CONST_DATE = '{DATE}';

            // --- variables

            var auth = options.auth;
            var counter = options.counter;
            var comment_list = options.comment_list;
            var comment_label = options.comment_label;
            var comment_inline = options.comment_inline;
            var comment_add = options.comment_add;

            // --- functions

            self.added = function() {

            };

            // --- actions

            console.log('comment_add', comment_add.length);

            if (comment_inline && comment_inline.length) {
                comment_inline.bind('keyup', function(event) {
                    if ($.trim(comment_inline.val()).length === 0) {
                        return;
                    }

                    switch (event.keyCode) {
                        case 13: // enter
                            self.addComment({
                                author: 'Me',
                                my: 1,
                                text: comment_inline.val()
                            });

                            comment_inline.val('');

                            if (self.added) {
                                self.added.call(self, self.lastComment());
                            }
                            break;

                        default:
                            break;
                    }
                });
            }

            if (comment_add && comment_add.length) {
                comment_add.bind('click', function() {
                    if ($.trim(comment_inline.val()).length === 0) {
                        return;
                    }

                    console.log('comment_add', window.fb_MyInfo, window.fb_FriendsInfo);

                    var action = function() {
                        console.log('action');

                        self.addComment({
                            author: 'Me',
                            my: 1,
                            text: comment_inline.val()
                        });

                        comment_inline.val('');

                        if (self.added) {
                            self.added.call(self, self.lastComment());
                        }
                    };

                    if (auth) {

                        if (window.fb_FriendsInfo) {

                        } else {

                        }

                        if (window.fb_MyInfo) {
                            action();
                        } else {
                            console.log('window.fb_connection', window.fb_connection);

                            window.fb_connection.getStatus(function(ok) {
                                console.log('status', ok);

                                var friendsInfo = function(response) {
                                    console.log('friendsInfo', response);

                                    window.fb_FriendsInfo = response;
                                };

                                var myInfo = function(response) {
                                    console.log('myInfo', response);

                                    window.fb_MyInfo = response;

                                    window.fb_connection.getFriendsInfo(friendsInfo);

                                    action();
                                };

                                if (ok) {
                                    window.fb_connection.getMyInfo(myInfo);
                                } else {
                                    window.fb_connection.login(function(ok) {
                                        console.log('login', ok);

                                        if (ok) {
                                            window.fb_connection.getMyInfo(myInfo);
                                        } else {

                                        }
                                    });
                                }
                            });
                        }
                    } else {
                        action();
                    }

                    return false;
                });
            }

            // --- methods

            self.template = function() {
                return ['<li>',
                    '<div class="m-comment-user-cover">',
                    '<img src="', CONST_BASE_URL, 'img/ic_user.png">',
                    '</div>',
                    '<div class="m-comment-user-message">',
                    '<div class="m-comment-user-author">', CONST_AUTHOR, '</div>',
                    '<div class="m-comment-user-text">', CONST_TEXT, '</div>',
                    '<div class="m-comment-user-data">', CONST_DATE, '</div>',
                    '</div>',
                    '</li>'].join('');
            };

            self.addComment = function(comment) {
                comment = comment || {};

                var result = '';

                comment.id = comment.id || ('comment_new_' + self.size());
                comment.base_url = comment.base_url || GLOBAL_PREFIX;
                comment.cover = comment.cover || '';
                comment.author = comment.author || 'Unknown';
                comment.text = comment.text || comment_inline.val();
                comment.date = comment.date || self.date(new Date().getTime());
                comment.my = comment.my || 0;

                var _id = comment.id;
                var _base_url = comment.base_url;
                var _cover = comment.cover;
                var _author = comment.author;
                var _text = comment.text;
                var _date = comment.date;
                var _my = comment.my;

                // ---

                _lastComment = comment;

                // ---

                var _template = $(self.template()
                        .replace(CONST_BASE_URL, _base_url)
                        .replace(CONST_AUTHOR, _author)
                        .replace(CONST_TEXT, _text)
                        .replace(CONST_DATE, _date));

                _template.attr('id', _id);
                _template.addClass(function() {
                    return _my ? 'comment_my' : '';
                });

                comment_list.append(_template);

                self.check();

                return result;
            };

            self.removeComment = function(comment) {
                comment = comment || {};
            };
            self.removeAll = function() {
                comment_list.empty();
            };

            self.lastComment = function() {
                return _lastComment;
            };
            self.size = function() {
                return comment_list.find('li').size();
            };
            self.check = function() {
                var _count = self.size();
                
                comment_label.toggle(_count === 0);

                counter.html(' (' + _count + ') ');
            };
            self.date = function(time) {
                moment.lang('en');
                return moment(new Date(time)).format('LLLL');
            };
            self.fromJSON = function(json) {
                json = json || {};
                if (json && json.hasOwnProperty('comments') && json.comments.length) {
                    self.removeAll();

                    $.each(json.comments, function(index, comment) {
                        var user = comment.user || {};

                        self.addComment({
                            text: comment.comment,
                            date: self.date(comment.created * 1000),
                            id: 'comment_' + comment.id,
                            author: $.trim((user.first_name || '') + ' ' + (user.last_name || ''))
                        });
                    });
                }
            };
            self.toJSON = function() {
                var result = '';

                return result;
            };

            return self;
        };

        // --- use

        var comments = new Comments({
            auth: false,
            counter: $('.m-action-comment .m-text span'),
            comment_list: $('#m-comments .m-comment-list'),
            comment_label: $('#m-comments .m-comment-label'),
            comment_inline: $('#m-comments .m-comment-inline input'),
            comment_add: $('#m-comments .m-comment-add')
        });

        comments.added = function(comment) {
            console.log('comment', comment);
            if (comment) {
                var request = {
                    action: 'proto.addComment',
                    pid: GLOBAL_PID || 0,
                    comment: comment.text,
                    test: 1
                };

                $.post(GLOBAL_SERVER_URL, request, function(data) {
                    //console.log('data', data);

                    comments.fromJSON(data);
                });
            }
        };

        $.post(GLOBAL_SERVER_URL, {action: 'proto.getComments', pid: GLOBAL_PID || 0, test: 1}, function(data) {
            //console.log('data', data);

            comments.fromJSON(data);
        });
    });

})(jQuery);