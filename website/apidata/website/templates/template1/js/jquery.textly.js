(function($) {

    "use strict";

    /** 
     * position: 
     * 
     * center,
     * leftTop,
     * rightTop,
     * leftBottom,
     * rightBottom
     * 
     * effect:
     * 
     * 
     * @param {object} config
     */
    $.fn.textly = function(config) {
        config = config || {};

        // ---

        var self = this;

        // ---  

        var _config = {
            container: 'body',
            randomize: false,
            fadeInSpeed: 1000,
            fadeOutSpeed: 1000,
            animationSpeedMin: 2000,
            animationSpeedMax: 3000,
            delay: 0,
            sleep: 0,
            effects: [],
            loopEffects: true,
            effect: {
                position: 'center',
                name: '',
                fadeInSpeed: 1000,
                fadeOutSpeed: 1000,
                animationSpeedMin: 2000,
                animationSpeedMax: 3000,
                delay: 0,
                sleep: 0
            },
            loaded: function() {
            },
            beforeFadeIn: function() {
            },
            afterFadeIn: function() {
            },
            beforeFadeOut: function() {
            },
            afterFadeOut: function() {
            }
        };

        // ---

        $.extend(_config, config);

        // ---

        var css3 = $.support.cssProperty('transition') && $.support.cssProperty('transform');

        // --- variables

        var _texts = [];
        var _container = $(_config.container);
        var _randomize = _config.randomize;
        var _fadeInSpeed = _config.fadeInSpeed;
        var _fadeOutSpeed = _config.fadeOutSpeed;
        var _animationSpeedMin = _config.animationSpeedMin;
        var _animationSpeedMax = _config.animationSpeedMax;
        var _delay = _config.delay;
        var _sleep = _config.sleep;
        var _effects = _config.effects || [];
        var _loopEffects = _config._loopEffects;

        var _index = 0;

        // --- methods
        self.start = function(index, count) {
            try {
                var slide = null;

                self.hide().each(function(i, e) {
                    var data_index = $(e).attr('data-index');

                    if (data_index === 'first' && index === 0 || data_index === 'last' && index === count - index || data_index === ('' + index)) {
                        slide = $(e);

                        return false;
                    } else {

                    }
                });

                if (slide) {
                    var html = slide.html();

                    //console.log('html', html.match(/<(?:.|\n)*?>/igm).length);

                    if (css3 && html.match(/<(?:.|\n)*?>/igm).length === 0) {
                        slide.textillate({
                            // the default selector to use when detecting multiple texts to animate
                            selector: '.texts',
                            // enable looping
                            loop: true,
                            // sets the minimum display time for each text before it is replaced
                            minDisplayTime: _animationSpeedMin,
                            // sets the initial delay before starting the animation
                            // (note that depending on the in effect you may need to manually apply 
                            // visibility: hidden to the element before running this plugin)
                            initialDelay: 0,
                            // set whether or not to automatically start animating
                            autoStart: true,
                            // custom set of 'in' effects. This effects whether or not the 
                            // character is shown/hidden before or after an animation  
                            inEffects: ['hinge'],
                            // custom set of 'out' effects
                            outEffects: ['hinge'],
                            // in animation settings
                            _in: {
                                // set the effect name
                                effect: 'fadeInLeft',
                                // set the delay factor applied to each consecutive character
                                delayScale: 1.5,
                                // set the delay between each character
                                delay: 50,
                                // set to true to animate all the characters at the same time
                                sync: true,
                                // randomize the character sequence 
                                // (note that shuffle doesn't make sense with sync = true)
                                shuffle: false,
                                // reverse the character sequence 
                                // (note that reverse doesn't make sense with sync = true)
                                reverse: false,
                                // callback that executes once the animation has finished
                                callback: function() {
                                }
                            },
                            // out animation settings.
                            _out: {
                                effect: 'fadeOutRight',
                                delayScale: 1.5,
                                delay: 50,
                                sync: true,
                                shuffle: false,
                                reverse: false,
                                callback: function() {
                                    self.hide();
                                }
                            },
                            // callback that executes once textillate has finished 
                            callback: function() {

                            }
                        }).fadeIn();
                    } else {
                        slide.fadeIn(_fadeInSpeed).delay(RANDOM(_animationSpeedMin, _animationSpeedMax)).fadeOut(_fadeOutSpeed);
                    }
                }
            } catch (e) {
                alert('textly' + e.message);
            }

            console.log('start index', index, 'count', count);
        };

        self.stop = function() {

        };

        // ---

        return self;
    };
}(jQuery));