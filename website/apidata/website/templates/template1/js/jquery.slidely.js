(function($) {

    "use strict";

    /**
     * effect:
     * 
     * auto,
     * 
     * point
     * pointIn,
     * pointOut,
     * 
     * points,
     * pointsIn,
     * pointsOut,
     * 
     * classicIn,
     * classicOut,
     * 
     * classicInRotate,
     * classicOutRotate,
     * 
     * zoomIn,
     * zoomOut,
     * 
     * zoomInRotate,
     * zoomOutRotate,
     * 
     * leftTopIn,
     * rightTopIn,
     * leftBottomIn,
     * rightBottomIn,
     * 
     * leftTopInRotate,
     * rightTopInRotate,
     * leftBottomInRotate,
     * rightBottomInRotate,
     * 
     * leftTopOut,
     * rightTopOut,
     * leftBottomOut,
     * rightBottomOut,
     * 
     * leftRightTop,
     * leftRightCenter, 
     * leftRightBottom,
     * 
     * rightLeftTop,
     * rightLeftCenter,
     * rightLeftBottom,
     * 
     * topBottomLeft,
     * topBottomCenter,
     * topBottomRight,
     * 
     * bottomTopLeft,
     * bottomTopCenter,
     * bottomTopRight,
     * 
     * random
     * 
     * @param {object} config
     */
    $.fn.slidely = function(config) {
        config = config || {};

        // ---

        var self = this;

        // --- 

        var _effectsList = [
            'auto',
            'point',
            'pointIn',
            'pointOut',
            'points',
            'pointsIn',
            'pointsOut',
            'classicIn',
            'classicOut',
            'classicInRotate',
            'classicOutRotate',
            'zoomIn',
            'zoomOut',
            'zoomInRotate',
            'zoomOutRotate',
            'leftTopIn',
            'rightTopIn',
            'leftBottomIn',
            'rightBottomIn',
            'leftTopInRotate',
            'rightTopInRotate',
            'leftBottomInRotate',
            'rightBottomInRotate',
            'leftTopOut',
            'rightTopOut',
            'leftBottomOut',
            'rightBottomOut',
            'leftTopOutRotate',
            'rightTopOutRotate',
            'leftBottomOutRotate',
            'rightBottomOutRotate',
            'leftRightTop',
            'leftRightCenter',
            'leftRightBottom',
            'rightLeftTop',
            'rightLeftCenter',
            'rightLeftBottom',
            'topBottomLeft',
            'topBottomCenter',
            'topBottomRight',
            'bottomTopLeft',
            'bottomTopCenter',
            'bottomTopRight',
            'random'
        ];

        var _config = {
            container: 'body',
            preload: 4,
            crossfade: true,
            randomize: false,
            loop: true,
            autoplay: true,
            autoload: true,
            effects: [],
            loopEffects: true,
            effect: {
                name: 'auto',
                fadeInSpeed: 1500,
                fadeOutSpeed: 1500,
                animationSpeedMin: 3000,
                animationSpeedMax: 4000,
                minScale: 1.2,
                maxScale: 1.5,
                minAngle: -5,
                maxAngle: 5,
                reverse: false,
                rotate: false,
                boost: false,
                boostPoints: [],
                scale: false,
                delay: 0,
                sleep: 0
            },
            loaded: function() {
            },
            prepare: function() {
            },
            beforeFadeIn: function() {
            },
            afterFadeIn: function() {
            },
            beforeFadeOut: function() {
            },
            afterFadeOut: function() {
            },
            start: function() {
            },
            end: function() {
            }
        };

        // ---

        $.extend(_config, config);

        // --- variables

        var beforeTransition = null;

        var currentTransition = null;

        var css3 = $.support.cssProperty('transition') && $.support.cssProperty('transform');
        //css3 = false;

        var _images = [];
        var _container = $(_config.container);
        var _preload = _config.preload;
        var _randomize = _config.randomize;
        var _loop = _config.loop;
        var _autoplay = _config.autoplay;
        var _autoload = _config.autoload;
        var _effects = _config.effects || [];
        var _effect = _config.effect || {};
        var _loopEffects = _config._loopEffects;

        var _index = 0;
        var _playing = false;
        var _prepare = false;
        var _loaded = false;

        // --- functions

        var _load = function(index, img) {
            _images[index] = {
                img: img,
                src: $(img).attr('src'),
                w: img.naturalWidth,
                h: img.naturalHeight,
                success: img.naturalWidth > 0 && img.naturalHeight > 0
            };

            var count = 0, len = self.length;

            for (var i = 0; i < len; i++) {
                if (_images[i]) {
                    $(_images[i].img).hide().remove();

                    count++;
                }
            }

            if ((_preload === 0 && count === _images.length) || _preload <= count || _preload >= self.length) {
                if (!_prepare && _config && _config.prepare) {
                    _prepare = true;

                    _config.prepare.call(self, _images);
                }

                if (_autoplay) {
                    _playing = true;

                    _check();
                }
            }

            if (count === _images.length) {
                if (!_loaded && _config && _config.loaded) {
                    _loaded = true;

                    _config.loaded.call(self, _images);
                }
            }
        };

        var _positioning = function(type) {
            if (_playing) {
                return;
            }

            var offset = 0;

            var containerWidth = _container.width();
            var containerHeight = _container.height();

            switch (type) {
                case 'prev':
                    offset = containerWidth;
                    break;
                case 'next':
                    offset = -containerWidth;
                    break;
                default:
                    break;
            }

            _index = (_index + 1) % _images.length;

            var image = _images[_index];

            var imageWidth = image.w;
            var imageHeight = image.h;

            var size = PROPORTION_POINT([imageWidth, imageHeight], [containerWidth, containerHeight], true);

            var img = $(image.img).css({
                'width': size[0],
                'height': size[1],
                'position': 'absolute',
                'left': (containerWidth - size[0]) / 2,
                'top': (containerHeight - size[1]) / 2
            }).show();

            $('.slide .image').transition({
                x: offset + 'px',
                queue: false,
                duration: 500,
                easing: 'in-out',
                complete: function() {
                    $(this).remove();
                }
            });

            $('<div class="slide"><div class="image"></div></div>').appendTo(_container).find('.image').css({
                'left': -offset + 'px'
            }).transition({
                x: offset + 'px',
                queue: false,
                duration: 500,
                easing: 'in-out',
                complete: function() {

                    $(this).css({
                        'transform': '',
                        'left': '0'
                    });
                }
            }).html(img);
        };

        var _check = function() {
            if (!_playing) {
                return;
            }

            if (!_images || !_images.length) {
                return;
            }

            // ---

            var index = _index;

            var image = _images[index];

            var img = $(image.img);

            var slide = $('<div class="slide"><div class="image"></div></div>').appendTo(_container);

            slide.find('.image').html(img);

            var imageWidth = image.w;
            var imageHeight = image.h;

            var imageRation = imageWidth / imageHeight;

            var containerWidth = _container.width();
            var containerHeight = _container.height();

            var containerRation = containerWidth / containerHeight;

            if (currentTransition) {
                beforeTransition = currentTransition;
            }

            currentTransition = {
                index: index,
                img: img,
                image: image
            };

            var effect = $.extend({}, _effect, (_effects && _effects.length && index < _effects.length) ? _effects[index] : _loopEffects && _effects && _effects.length ? _effects[index % _effects.length] : _effect);

            console.log('effect', effect);

            effect = (effect.name === 'random' || _effectsList.indexOf(effect.name) === -1) ? $.extend({}, effect, {name: _effectsList[RANDOM(0, _effectsList.length)]}) : effect;

            console.log('effect.name', effect.name);

            if (css3) {
                if (effect.name === 'auto') {
                    var arr = ['zoomIn', 'zoomOut'];

                    if (imageRation > containerRation) { // изображение шире области отрисовки
                        arr = [
                            'leftRightTop',
                            'leftRightCenter',
                            'leftRightBottom',
                            'zoomIn',
                            'zoomOut',
                            'zoomInRotate',
                            'zoomOutRotate',
                            'rightLeftTop',
                            'rightLeftCenter',
                            'rightLeftBottom',
                            'point',
                            'pointIn',
                            'pointOut',
                            'points',
                            'pointsIn',
                            'pointsOut',
                            'classicIn',
                            'classicOut',
                            'classicInRotate',
                            'classicOutRotate',
                            'leftTopIn',
                            'rightTopIn',
                            'leftBottomIn',
                            'rightBottomIn',
                            'leftTopInRotate',
                            'rightTopInRotate',
                            'leftBottomInRotate',
                            'rightBottomInRotate',
                            'leftTopOut',
                            'rightTopOut',
                            'leftBottomOut',
                            'rightBottomOut',
                            'leftTopOutRotate',
                            'rightTopOutRotate',
                            'leftBottomOutRotate',
                            'rightBottomOutRotate'
                        ];
                    } else if (imageRation < containerRation) { // изображение выше области отрисовки
                        arr = [
                            'topBottomLeft',
                            'topBottomCenter',
                            'topBottomRight',
                            'zoomIn',
                            'zoomOut',
                            'zoomInRotate',
                            'zoomOutRotate',
                            'bottomTopLeft',
                            'bottomTopCenter',
                            'bottomTopRight',
                            'point',
                            'pointIn',
                            'pointOut',
                            'points',
                            'pointsIn',
                            'pointsOut',
                            'classicIn',
                            'classicOut',
                            'classicInRotate',
                            'classicOutRotate',
                            'leftTopIn',
                            'rightTopIn',
                            'leftBottomIn',
                            'rightBottomIn',
                            'leftTopInRotate',
                            'rightTopInRotate',
                            'leftBottomInRotate',
                            'rightBottomInRotate',
                            'leftTopOut',
                            'rightTopOut',
                            'leftBottomOut',
                            'rightBottomOut',
                            'leftTopOutRotate',
                            'rightTopOutRotate',
                            'leftBottomOutRotate',
                            'rightBottomOutRotate'
                        ];
                    } else { // изображение и область имеют пропорциональное совпадение

                    }

                    effect.name = arr[RANDOM(0, arr.length - 1)];
                }
            } else {
                effect.name = 'zoomIn';
            }

            console.log('effect', effect, 'imageRation', imageRation, 'containerRation', containerRation);

            var _name = effect.name; // use
            var _reverse = effect.reverse; // not use
            var _rotate = effect.rotate; // use
            var _scale = effect.scale; // not use
            var _boost = effect.boost; // not use
            var _boostPoints = effect.boostPoints || []; // not use
            var _delay = effect.delay; // not use
            var _sleep = effect.sleep; // not use
            var _fadeInSpeed = effect.fadeInSpeed; // use
            var _fadeOutSpeed = effect.fadeOutSpeed; // use
            var _animationSpeedMin = effect.animationSpeedMin; // use
            var _animationSpeedMax = effect.animationSpeedMax; // use
            var _minScale = effect.minScale; // use
            var _maxScale = effect.maxScale; // use
            var _minAngle = effect.minAngle; // not use
            var _maxAngle = effect.maxAngle; // not use
            var _crossfade = _config.crossfade; // not use

            // --- Scale, Move, Rotate, ... options

            var size = PROPORTION_POINT([imageWidth, imageHeight], [containerWidth, containerHeight], false);

            //var k = PROPORTION_RATIO([imageWidth, imageHeight], [containerWidth, containerHeight], false);
            //console.log(k, imageWidth / k, imageHeight / k);

            var rect1 = [0, 0, size[0], size[1]];
            var rect2 = SCALE_RECT(rect1, RANDOM_F(_minScale, _maxScale));

            // --- 

            var sizeXMin = 0, sizeYMin = 0, sizeXMax = 0, sizeYMax = 0, lenX = 0, lenY = 0, fromX = 0, fromY = 0, toX = 0, toY = 0, fromWidth = 0, fromHeight = 0, toWidth = 0, toHeight = 0, fromScale = 1, toScale = 1, fromAngle = 0, toAngle = 0;

            // ---
            /*
             * effect:
             * 
             * auto,
             * 
             * point
             * pointIn
             * pointOut
             * points
             * pointsIn
             * pointsOut
             * 
             * classicIn,
             * classicOut,
             * 
             * classicInRotate,
             * classicOutRotate,
             * 
             * zoomIn,
             * zoomOut,
             * 
             * zoomInRotate,
             * zoomOutRotate,
             * 
             * leftTopIn,
             * rightTopIn,
             * leftBottomIn,
             * rightBottomIn,
             * 
             * leftTopInRotate,
             * rightTopInRotate,
             * leftBottomInRotate,
             * rightBottomInRotate,
             * 
             * leftTopOut,
             * rightTopOut,
             * leftBottomOut,
             * rightBottomOut,
             * 
             * leftTopOutRotate,
             * rightTopOutRotate,
             * leftBottomOutRotate,
             * rightBottomOutRotate,
             * 
             * leftRightTop,
             * leftRightCenter, 
             * leftRightBottom,
             * 
             * rightLeftTop,
             * rightLeftCenter,
             * rightLeftBottom,
             * 
             * topBottomLeft,
             * topBottomCenter,
             * topBottomRight,
             * 
             * bottomTopLeft,
             * bottomTopCenter,
             * bottomTopRight,
             * 
             * random
             */

            sizeXMin = (rect1[2] - containerWidth); // отступ по ширене без увеличение
            sizeYMin = (rect1[3] - containerHeight); // отступ по высоте без увеличение

            sizeXMax = (rect2[2] - containerWidth); // отступ по ширене с увеличение
            sizeYMax = (rect2[3] - containerHeight); // отступ по высоте с увеличение

            lenX = rect2[2] - rect1[2]; // разница по ширене стартовой позиции и конечной
            lenY = rect2[3] - rect1[3]; // разница по высоте стартовой позиции и конечной

            var _name = ['zoomInRotate'][0];
            //var _name = ['classicInRotate', 'classicOutRotate'][RANDOM(0, 1)];

            switch (_name) {
                case 'point': // прохождение по слуяайным точкам
                case 'pointIn':
                case 'pointOut':
                case 'points': // пройти по точкам (заданным в boostPoints)
                case 'pointsIn':
                case 'pointsOut':

                    var points = _boostPoints || [[]];

                    if (['point', 'points'].indexOf(_name) !== -1) {
                        fromWidth = rect1[2];
                        fromHeight = rect1[3];

                        toWidth = rect2[2];
                        toHeight = rect2[3];
                    }

                    if (['pointIn', 'pointsIn'].indexOf(_name) !== -1) {
                        fromWidth = rect1[2];
                        fromHeight = rect1[3];

                        toWidth = rect2[2];
                        toHeight = rect2[3];
                    }

                    if (['pointOut', 'pointsOut'].indexOf(_name) !== -1) {
                        fromWidth = rect2[2];
                        fromHeight = rect2[3];

                        toWidth = rect1[2];
                        toHeight = rect1[3];
                    }
                    break;

                case 'classicIn': // классический вариант     
                case 'classicOut': // классический вариант
                case 'classicInRotate':
                case 'classicOutRotate':

                    if (_name === 'classicIn' || _name === 'classicInRotate') {
                        fromWidth = rect1[2];
                        fromHeight = rect1[3];

                        toWidth = rect2[2];
                        toHeight = rect2[3];

                        if (_name === 'classicInRotate') {
                            fromAngle = RANDOM_F(_minAngle, _maxAngle);
                        }
                    } else if (_name === 'classicOut' || _name === 'classicOutRotate') {
                        fromWidth = rect2[2];
                        fromHeight = rect2[3];

                        toWidth = rect1[2];
                        toHeight = rect1[3];

                        if (_name === 'classicOutRotate') {
                            toAngle = RANDOM_F(_minAngle, _maxAngle);
                        }
                    } else {

                    }

                    fromX = RANDOM(-sizeXMin, 0);
                    fromY = RANDOM(-sizeYMin, 0);

                    toX = (RANDOM(0, 1) ? sizeXMax : 0) - ((lenX / 2) + fromX + sizeXMin);
                    toY = (RANDOM(0, 1) ? sizeYMax : 0) - ((lenY / 2) + fromY + sizeYMin);

                    //toX = sizeXMax - ((lenX / 2) + fromX + sizeXMin);
                    //toY = sizeYMax - ((lenY / 2) + fromY + sizeYMin);

                    //toX = (-lenX / 2) - fromX - sizeXMin;
                    //toY = (-lenY / 2) - fromY - sizeYMin;

                    toScale = toWidth / fromWidth;

                    break;

                case 'leftTopIn':
                case 'rightTopIn':
                case 'leftBottomIn':
                case 'rightBottomIn':

                case 'leftTopInRotate':
                case 'rightTopInRotate':
                case 'leftBottomInRotate':
                case 'rightBottomInRotate':

                case 'leftTopOut':
                case 'rightTopOut':
                case 'leftBottomOut':
                case 'rightBottomOut':

                case 'leftTopOutRotate':
                case 'rightTopOutRotate':
                case 'leftBottomOutRotate':
                case 'rightBottomOutRotate':

                    if (['leftTopIn', 'rightTopIn', 'leftBottomIn', 'rightBottomIn', 'leftTopInRotate', 'rightTopInRotate', 'leftBottomInRotate', 'rightBottomInRotate'].indexOf(_name) !== -1) {
                        fromWidth = rect1[2];
                        fromHeight = rect1[3];

                        toWidth = rect2[2];
                        toHeight = rect2[3];

                        if (['leftTopInRotate', 'rightTopInRotate', 'leftBottomInRotate', 'rightBottomInRotate'].indexOf(_name) !== -1) {
                            //fromAngle = RANDOM_F(_minAngle, _maxAngle);
                        } else {

                        }
                    }

                    if (['leftTopOut', 'rightTopOut', 'leftBottomOut', 'rightBottomOut', 'leftTopOutRotate', 'rightTopOutRotate', 'leftBottomOutRotate', 'rightBottomOutRotate'].indexOf(_name) !== -1) {
                        fromWidth = rect2[2];
                        fromHeight = rect2[3];

                        toWidth = rect1[2];
                        toHeight = rect1[3];

                        if (['leftTopOutRotate', 'rightTopOutRotate', 'leftBottomOutRotate', 'rightBottomOutRotate'].indexOf(_name) !== -1) {
                            //fromAngle = RANDOM_F(_minAngle, _maxAngle);
                        } else {

                        }
                    }

                    // leftTop
                    fromX = -sizeXMin / 2 + RANDOM(-sizeXMin / 2, 0);
                    fromY = -sizeYMin / 2 + RANDOM(-sizeYMin / 2, 0);

                    toX = (lenX / 2) - fromX;
                    toY = (lenY / 2) - fromY;

                    // rightTop
                    fromX = RANDOM(-sizeXMin / 2, 0);
                    fromY = -sizeYMin / 2 + RANDOM(-sizeYMin / 2, 0);

                    toX = -sizeXMax + (lenX / 2) - fromX;
                    toY = (lenY / 2) - fromY;

                    // leftBottom
                    fromX = -sizeXMin / 2 + RANDOM(-sizeXMin / 2, 0);
                    fromY = RANDOM(-sizeYMin / 2, 0);

                    toX = (lenX / 2) - fromX;
                    toY = -sizeYMax + (lenY / 2) - fromY;

                    // rightBottom  
                    fromX = RANDOM(-sizeXMin / 2, 0);
                    fromY = RANDOM(-sizeYMin / 2, 0);

                    toX = -sizeXMax + (lenX / 2) - fromX;
                    toY = -sizeYMax + (lenY / 2) - fromY;

                    toScale = toWidth / fromWidth;

                    break;

                case 'zoomIn': // увеличение
                case 'zoomInRotate':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = rect2[2];
                    toHeight = rect2[3];

                    fromX = (containerWidth - fromWidth) / 2;
                    fromY = (containerHeight - fromHeight) / 2;

                    toX = 0;
                    toY = 0;

                    toScale = toWidth / fromWidth;

                    if (_name === 'zoomInRotate') {
                        toAngle = RANDOM_F(_minAngle, _maxAngle);
                    }

                    break;

                case 'zoomOut': // уменьшение
                case 'zoomOutRotate':
                    fromWidth = rect2[2];
                    fromHeight = rect2[3];

                    toWidth = rect1[2];
                    toHeight = rect1[3];

                    fromX = (containerWidth - fromWidth) / 2;
                    fromY = (containerHeight - fromHeight) / 2;

                    toX = 0;
                    toY = 0;

                    if (_name === 'zoomOutRotate') {
                        fromAngle = RANDOM_F(_minAngle, _maxAngle);
                    }

                    toScale = toWidth / fromWidth;

                    break;

                case 'leftRightTop': // движение слева на право
                case 'leftRightCenter':
                case 'leftRightBottom':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'leftRightTop') {
                        fromY = 0;
                    } else if (effect === 'leftRightCenter') {
                        fromY = -(toHeight - containerHeight) / 2;
                    } else if (effect === 'leftRightBottom') {
                        fromY = -(toHeight - containerHeight);
                    }

                    fromX = -(sizeXMax - lenX);

                    toX = -fromX;
                    toY = 0;
                    break;

                case 'rightLeftTop': // движение справа налево
                case 'rightLeftCenter':
                case 'rightLeftBottom':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'rightLeftTop') {
                        fromY = 0;
                    } else if (effect === 'rightLeftCenter') {
                        fromY = -(toHeight - containerHeight) / 2;
                    } else if (effect === 'rightLeftBottom') {
                        fromY = -(toHeight - containerHeight);
                    }

                    fromX = 0;

                    toX = -(sizeXMax - lenX);
                    toY = 0;
                    break;

                case 'topBottomLeft': // движение сверху вниз
                case 'topBottomCenter':
                case 'topBottomRight':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'topBottomLeft') {
                        fromX = 0;
                    } else if (effect === 'topBottomCenter') {
                        fromX = -(toWidth - containerWidth) / 2;
                    } else if (effect === 'topBottomRight') {
                        fromX = -(toWidth - containerWidth);
                    }

                    fromY = -(sizeYMax - lenY);

                    toX = 0;
                    toY = -fromY;
                    break;

                case 'bottomTopLeft': // движение свниз вверх
                case 'bottomTopCenter':
                case 'bottomTopRight':
                    fromWidth = rect1[2];
                    fromHeight = rect1[3];

                    toWidth = fromWidth;
                    toHeight = fromHeight;

                    if (effect === 'bottomTopLeft') {
                        fromX = 0;
                    } else if (effect === 'bottomTopCenter') {
                        fromX = -(toWidth - containerWidth) / 2;
                    } else if (effect === 'bottomTopRight') {
                        fromX = -(toWidth - containerWidth);
                    }

                    fromY = 0;

                    toX = 0;
                    toY = -(sizeYMax - lenY);

                    break;
                default:
                    break;
            }

            if (index === 0) {
                if (_config && _config.start) {
                    _config.start.call(self, _images);
                }
            }

            console.log('size', size, 'containerWidth', containerWidth, 'containerHeight', containerHeight, 'fromWidth', fromWidth, 'fromHeight', fromHeight, 'toWidth', toWidth, 'toHeight', toHeight, 'fromX', fromX, 'fromY', fromY, 'toX', toX, 'toY', toY);

            // --- Animation

            var fadeInAnimation = function() {
                if (!_playing) {
                    return;
                }

                if (css3) {
                    if (_crossfade) {
                        img.show().parent().show().transition({
                            opacity: 1,
                            duration: _fadeInSpeed,
                            complete: fadeInComplete
                        });
                    } else {
                        img.show().parent().show().transition({
                            opacity: 1,
                            duration: _fadeInSpeed,
                            complete: fadeInComplete
                        });
                    }

                } else {
                    img.show().parent().hide().css({
                        opacity: 1
                    }).fadeIn(_fadeInSpeed, fadeInComplete);
                }
            };

            var fadeOutAnimation = function() {
                if (!_playing) {
                    return;
                }

                if (css3) {
                    if (_crossfade) {
                        img.parent().transition({
                            opacity: 0,
                            duration: _fadeOutSpeed,
                            complete: fadeOutComplete
                        });
                    } else {
                        img.transition({
                            opacity: 0,
                            duration: _fadeOutSpeed,
                            complete: fadeOutComplete
                        });
                    }
                } else {
                    img.parent().fadeOut(_fadeOutSpeed, fadeOutComplete);
                }
            };

            var transitionAnimation = function() {
                if (!_playing) {
                    return;
                }

                var duration = RANDOM(_animationSpeedMin, _animationSpeedMax);

                if (css3) {
                    img.transition({
                        x: toX + 'px',
                        y: toY + 'px',
                        scale: toScale,
                        rotate: toAngle,
                        queue: false,
                        duration: duration,
                        easing: 'in-out',
                        complete: transitionComplete
                    });
                } else {

                }

                if (_crossfade) {
                    currentTransition.timer = setTimeout(function() {
                        if (!_playing) {
                            return;
                        }

                        fadeOutAnimation();

                        _check();
                    }, duration - _fadeOutSpeed);
                } else {
                    currentTransition.timer = setTimeout(function() {
                        _check();
                    }, duration - _fadeOutSpeed);

                }
            };

            var fadeInComplete = function() {
                console.log('fadeInComplete');

                if (!_playing) {
                    return;
                }
                self.not(img).fadeOut('fast', function() {
                    $(this).parents('.slide').hide().remove();
                    $(this).hide().remove().removeAttr('style').hide();

                });
                if (_crossfade) {

                } else {
                    transitionAnimation();
                }

                if (_config && _config.afterFadeIn) {
                    _config.afterFadeIn.call(self, _images, image, index);
                }
            };

            var fadeOutComplete = function() {
                console.log('fadeOutComplete');

                if (!_playing) {
                    return;
                }

                if (_config && _config.afterFadeOut) {
                    _config.afterFadeOut.call(self, _images, image, index);
                }

                img.fadeOut('fast', function() {
                    $(this).parents('.slide').remove();
                    $(this).hide().remove().removeAttr('style').hide();
                });
            };

            var transitionComplete = function() {
                console.log('transitionComplete');

                if (!_playing) {
                    return;
                }

                if (_loop || index !== _images.length - 1) {
                    if (_config && _config.beforeFadeOut) {
                        _config.beforeFadeOut.call(self, _images, image, index);
                    }
                    img.css({
                        'z-index': '-1'
                    });

                    if (_crossfade) {

                    } else {
                        fadeOutAnimation();

                        if (_playing) {
                            _check();
                        }
                    }
                } else {
                    if (_config && _config.end) {
                        _playing = false;

                        _config.end.call(self, _images);
                    }
                }
            };

            // ---

            if (_config && _config.beforeFadeIn) {
                _config.beforeFadeIn.call(self, _images, image, index);
            }

            img.css({
                'width': fromWidth + 'px',
                'height': fromHeight + 'px',
                'left': fromX + 'px',
                'top': fromY + 'px',
                'z-index': '-2',
                'position': 'absolute',
                'rotate': fromAngle
            }).parent().css({
                'opacity': '0'
            });

            fadeInAnimation();

            if (_crossfade) {
                transitionAnimation();
            } else {

            }

            // ---

            _index = (_index + 1) % _images.length;
        }; // check

        // --- methods

        self.start = function() {
            if (_playing) {
                return false;
            }

            _playing = true;

            _check();

            return true;
        };
        self.stop = function() {
            if (!_playing) {
                return false;
            }



            if (css3) {
                if (beforeTransition && beforeTransition.img) {
                    clearTimeout(beforeTransition.timer);

                    clearTimeout(beforeTransition.img.data('timer1'));
                    clearTimeout(beforeTransition.img.data('timer2'));

                    $().add(self).add(self.parent()).transition({
                        x: 0 + 'px',
                        y: 0 + 'px',
                        scale: 1,
                        rotate: 0,
                        queue: false,
                        duration: 24 * 60 * 60 * 1000 // 24h
                    });
                }

                if (currentTransition && currentTransition.img) {
                    clearTimeout(currentTransition.timer);

                    clearTimeout(currentTransition.img.data('timer1'));
                    clearTimeout(currentTransition.img.data('timer2'));

                    $().add(self).add(self.parent()).transition({
                        x: 0 + 'px',
                        y: 0 + 'px',
                        scale: 1,
                        rotate: 0,
                        queue: false,
                        duration: 24 * 60 * 60 * 1000 // 24h
                    });
                }
            } else {

            }

            _playing = false;

            return true;
        };

        self.isPlaying = function() {
            return _playing;
        };

        self.isPreload = function() {
            return _preload;
        };

        self.isLoaded = function() {
            return _loaded;
        };

        self.next = function() {
            console.log('!!! next');

            _positioning('next');
        };

        self.prev = function() {
            console.log('!!! prev');

            _positioning('prev');
        };

        self.position = function() {
            return _index;
        };

        self.count = function() {
            return self.length;
        };

        self.load = function() {
            self.each(function(index, img) {
                if (img.complete) {
                    _load(index, img);
                } else {
                    $(img).one('load', function() {
                        _load(index, img);
                    });
                }
            });
        };

        self.current = function() {
            return self.get(_index);
        };

        // ---

        _container.css({
            'position': 'relative',
            'overflow': 'hidden'
        });

        if (_randomize) {
            self.sort(function() {
                return 0.5 - Math.random();
            });
        }

        self.hide();

        if (_autoload) {
            self.load();
        }

        // ---

        return self;
    };

}(jQuery));