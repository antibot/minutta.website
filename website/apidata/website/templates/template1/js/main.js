// --- requestAnimationFrame

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
        window.requestAnimationFrame = window[vendors[i] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[i] + 'CancelAnimationFrame'] || window[vendors[i] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// --- FPS

var FPS = function(debug) {
    var self = this;

    var startTime = Date.now();
    var currentTime = startTime;
    var frames = 0;
    var lastFrames = 0;

    self.exec = function() {
        frames++;
        currentTime = Date.now();
        if (currentTime - startTime >= 1000) {
            //console.log('fps: ' + frames);

            debug.innerHTML = frames;

            lastFrames = frames;
            frames = 0;
            startTime = currentTime;
        }

        // ---

        return lastFrames;
    };

    return self;
};

// --- loop

(function($) {
    var autoPlaySlideshow = true;
    var autoPlaySound = false;
    window.initialized = false;
    var debug = document.getElementById('debug');
    var fps = new FPS(debug);
    (function loop() {
        fps.exec();
        // --- loop

        requestAnimationFrame(loop);
    })();
    $(function() {
        var player = null, audio = null, slidely = null, textly = null, actions = $('#actions'), sound = $('#sound');
        try {
            audiojs.events.ready(function() {
                player = audiojs.createAll();
                if (player) {
                    audio = player[0];
                    audio.loop = true;
                    console.log(audio);
                    audio.load(sound.attr('data-url'));
                }
            });
        } catch (e) {
            alert('audiojs' + e.message);
        }

        try {
            textly = $('#texts > div').textly({
                container: '#texts',
                beforeFadeIn: function(texts, text, index) {
                    console.log('textly beforeFadeIn', texts);
                },
                afterFadeIn: function(texts, text, index) {
                    console.log('textly afterFadeIn', texts);
                },
                beforeFadeOut: function(texts, text, index) {
                    console.log('textly beforeFadeOut', texts);
                },
                afterFadeOut: function(texts, text, index) {
                    console.log('textly afterFadeOut', texts);
                }
            });
        } catch (e) {
            alert('textly' + e.message);
        }

        try {
            slidely = $('#slides > img').slidely({
                container: '#slides',
                autoplay: false,
                loop: true,
                loaded: function(images) {
                    console.log('slidely loaded', images);
                    var self = this;
                    var interval = setInterval(function() { // all loaded
                        if (audio) {
                            clearInterval(interval);
                            /*
                             actions.fadeIn('fast').bind('click', function() {
                             actions.fadeOut('fast');
                             
                             self.start();
                             
                             audio.play();
                             });
                             */

                            if (autoPlaySlideshow) {
                                self.start();
                            }

                            if (autoPlaySound) {
                                audio.play();
                            }

                            window.initialized = true;
                        }
                    }, 100);
                },
                beforeFadeIn: function(images, image, index) {
                    console.log('slidely beforeFadeIn', images);
                },
                afterFadeIn: function(images, image, index) {
                    console.log('slidely afterFadeIn', images);
                    if (textly) {
                        textly.start(index, (images || []).length);
                    }
                },
                beforeFadeOut: function(images, image, index) {
                    console.log('slidely beforeFadeOut', images);
                },
                afterFadeOut: function(images, image, index) {
                    console.log('slidely afterFadeOut', images);
                },
                start: function(images) {
                    console.log('slidely start', images);
                },
                end: function(images) {
                    console.log('slidely end', images);
                    if (textly) {
                        textly.start((images || []).length - 1, (images || []).length);
                    }
                }
            });
        } catch (e) {
            alert('slidely' + e.message);
        }

        window.loaded = function() {
            return audio && textly && slidely && slidely.isLoaded();
        };
        window.play = function() {
            if (!window.loaded()) {
                return false;
            }

            slidely.start();
            audio.play();
            return true;
        };
        window.stop = function() {
            if (!window.loaded()) {
                return false;
            }

            slidely.stop();
            audio.pause();
            return true;
        };
        window.next = function() {
            if (!window.loaded()) {
                return false;
            }

            slidely.next();
            return true;
        };
        window.prev = function() {
            if (!window.loaded()) {
                return false;
            }

            slidely.prev();
            return true;
        };
        window.position = function() {
            return slidely.position();
        };
        window.count = function() {
            return slidely.count();
        };
        window.current = function() {
            return slidely.current();
        };
        Visibility.change(function(e, state) {
            console.log('state', state);

            switch (state) {
                case 'visible':
                    play();
                    break;
                case 'hidden':
                    stop();
                    break;
                case 'prerenderer':
                    stop();
                    break;
                default:
                    break;
            }

        });
    });
})(jQuery);