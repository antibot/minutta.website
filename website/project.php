<?php

$template = (isset($_REQUEST['template']) && !empty($_REQUEST['template'])) ? $_REQUEST['template'] : 0;

if (!empty($template)) {
    require_once "template" . ($template) . ".php";

    exit();
}

$version = (isset($_REQUEST['v']) && !empty($_REQUEST['v'])) ? $_REQUEST['v'] : 1;

switch ($version) {
    case 1:
        require_once 'project_1.php';
        break;
    case 2:
    case 3:
        require_once 'project_2.php';
        break;
    case 4:
        require_once 'website.php';
        break;
    default:
        require_once 'project_1.php';
        break;
}


