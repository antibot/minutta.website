﻿<?php
$LOCAL = $_SERVER['HTTP_HOST'] == 'localhost';

$PREFIX = ($LOCAL ? '' : '//' . $_SERVER['HTTP_HOST'] . '/') . 'apidata/website/templates/template1/';

// ---

$images = isset($_REQUEST['images']) && !empty($_REQUEST['images']) ? $_REQUEST['images'] : array();

$texts = isset($_REQUEST['texts']) && !empty($_REQUEST['texts']) ? $_REQUEST['texts'] : array();
$sound = isset($_REQUEST['sound']) && !empty($_REQUEST['sound']) ? $_REQUEST['sound'] : null;

if (empty($images)) {
    $images = array();

    //$images[] = (($LOCAL ? '' : '//' . $_SERVER['HTTP_HOST'] . '/') . 'apidata/website/') . "frames/" . (4) . ".JPG";
    //$images[] = (($LOCAL ? '' : '//' . $_SERVER['HTTP_HOST'] . '/') . 'apidata/website/') . "frames/" . (13) . ".JPG";

    for ($i = 0; $i < 13; $i++) {
        $images[] = (($LOCAL ? '' : '//' . $_SERVER['HTTP_HOST'] . '/') . 'apidata/website/') . "frames/" . ($i + 1) . ".JPG";
    }
}

if (empty($texts)) {
    $texts = array();

    $texts[] = array('index' => 'first', 'text' => '<small>1 <b>ЯНВАРЯ</b> 2014</small><br /><br />ПРОГУЛКА ПО<br />"ВЕТРЕННОМУ ХОЛМУ"<br /><br /><small>ПОД ONE DIRECTION</small><br />');
    $texts[] = array('index' => 'last', 'text' => '"МИНУТТА С ЛЮБОВЬЮ"<br /><br /><small>ПОД ONE DIRECTION</small><br />');
}

if (empty($sound)) {
    $sound = 'songs/1.mp3';
}
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 1.0, maximum-scale = 1.0">

        <title></title>
        <link rel="stylesheet" type="text/css" href="<?= $PREFIX; ?>css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?= $PREFIX; ?>css/animate.css" />
        <link rel="stylesheet" type="text/css" href="<?= $PREFIX; ?>css/style.css" />
    </head>
    <body>
        <div id="debug" class="debug"></div>

        <audio preload loop></audio>

        <div id="sound" data-url="<?= $sound; ?>"></div>

        <div id="slides">
            <?php foreach ($images as $image): ?>
                <img src="<?= $image; ?>" />
            <?php endforeach; ?>
        </div>

        <div class="table">
            <div class="tr">
                <div class="td">
                    <div id="texts">
                        <?php foreach ($texts as $text): ?>
                            <div data-index="<?= $text['index']; ?>">
                                <?= $text['text']; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div> 
        </div>

        <div id="actions"></div>

        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/utils.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.transit.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.fittext.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.lettering.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.textillate.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.slidely.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.textly.js"></script>
        <script type="text/javascript" src="<?= $PREFIX; ?>js/jquery.support.cssproperty.js"></script>

                <script type="text/javascript" src="<?= $PREFIX; ?>js/visibility.core.js"></script>
                <script type="text/javascript" src="<?= $PREFIX; ?>js/visibility.fallback.js"></script>
                <script type="text/javascript" src="<?= $PREFIX; ?>js/visibility.timers.js"></script>
        
        <script type="text/javascript" src="<?= $PREFIX; ?>js/audio/audio.js"></script>

        <script type="text/javascript" src="<?= $PREFIX; ?>js/main.js"></script>

    </body>
</html>